#!/bin/sh

echo
qmake --version
echo

g++ -v
echo

cmake --version
echo

DIR=`mktemp -d`
cd $DIR

echo "#include <QApplication>\n" \
     "#include <QPushButton>\n" \
     "" \
     "int main(int argc, char *argv[])\n" \
     "{\n" \
     "    QApplication a(argc, argv);\n" \
     "" \
     "    QPushButton button (\"Hello world !\");\n" \
     "    button.show();\n" \
     "" \
     "    return a.exec();\n" \
     "}\n" \
     > test.cpp

echo "QT += core gui widgets\n" \
     "TARGET = test\n" \
     "TEMPLATE = app\n" \
     "SOURCES += test.cpp\n" \
     > test.pro

qmake test.pro
make

echo "cmake_minimum_required(VERSION 3.10)" \
     "# set the project name" \
     "project(Tutorial)" \
     "set(CMAKE_INCLUDE_CURRENT_DIR ON)" \
     "# add the executable" \
     "find_package(Qt5Core REQUIRED)" \
     "find_package(Qt5Gui REQUIRED)" \
     "find_package(Qt5Widgets REQUIRED)" \
     "list(APPEND LIBRARIES" \
     "  Qt5::Core " \
     "  Qt5::Gui " \
     "  Qt5::Widgets " \
     "  Qt5::Network" \
     "  Qt5::PrintSupport)" \
     "list(REMOVE_DUPLICATES LIBRARIES)" \
     "add_executable(Tutorial test.cpp)" > CMakeList.txt

cmake . -DCMAKE_PREFIX_PATH=/opt/qt/5.12.3/gcc_64
make
